
DROP DATABASE IF EXISTS ejemplos_sql2;
CREATE DATABASE ejemplos_sql2;

\c ejemplos_sql2

create table empleade (legajo int, nombre text, apellido text, departamento text, salario int);
create table profesore (legajo int, nombre text, apellido text, departamento text, salario int);
create table proyecto (proyecto_id int, nombre text, departamento text, fecha_inicio date);
create table empleade_proyecto (legajo int, proyecto_id int);
create table materia (nombre text);
create table parcial (materia_id text, parcial_id int, etapa text, profesore_id int);

alter table empleade add constraint empleade_pk primary key (legajo);
alter table profesore add constraint profesore_pk primary key (legajo);
alter table proyecto add constraint proyecto_pk primary key (proyecto_id);
alter table empleade_proyecto add constraint empleade_proyecto_pk primary key (legajo, proyecto_id);
alter table materia add constraint materia_pk primary key (nombre);
alter table parcial add constraint parcial_pk primary key (parcial_id);

alter table empleade_proyecto add constraint empleade_proyecto_legajo_fk foreign key (legajo) references empleade (legajo);
alter table empleade_proyecto add constraint empleade_proyecto_proyecto_id_fk foreign key (proyecto_id) references proyecto (proyecto_id);
alter table parcial add constraint parcial_profesore_id_fk foreign key (profesore_id) references profesore (legajo);

insert into empleade values (1234, 'Lionel', 'Messi', 'sistemas', 500000);
insert into empleade values (2345, 'Rodrigo', 'De Paul', 'administracion', 100000);
insert into empleade values (3456, 'Lautaro', 'Martinez', 'sistemas', 200000);
insert into empleade values (4567, 'Julian', 'Alvarez', 'finanzas', 150000);
insert into empleade values (5678, 'Emiliano', 'Martinez', 'finanzas', 250000);
insert into empleade values (6789, 'Angel', 'Correa', 'administracion', 100000);
insert into empleade values (7890, 'Angel', 'Di Maria', 'sistemas', 400000);

insert into profesore values (2222, 'Daniel', 'Bertaccini', 'sistemas', 500000);
insert into profesore values (3333, 'Hernan', 'Rondelli', 'sistemas', 500000);

insert into parcial values ('Base de Datos', 1, 'Primer Parcial', 2222);
insert into parcial values ('Base de Datos', 2, 'Segundo Parcial', 2222);
insert into parcial values ('Base de Datos', 3, 'Final Parcial', 2222);
insert into parcial values ('Base de Datos', 4, 'Primer Parcial', 3333);
insert into parcial values ('Base de Datos', 5, 'Segundo Parcial', 3333);

insert into materia values ('Base de Datos');

insert into proyecto values (1, 'Inversiones off-shore', 'finanzas', '2018-08-12');
insert into proyecto values (2, 'Mejoras SIU', 'sistemas', '2023-03-01');

insert into empleade_proyecto values (1234, 1);
insert into empleade_proyecto values (4567, 1);
insert into empleade_proyecto values (5678, 1);
insert into empleade_proyecto values (1234, 2);
insert into empleade_proyecto values (3456, 2);
insert into empleade_proyecto values (7890, 2);
