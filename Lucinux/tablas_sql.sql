DROP DATABASE IF EXISTS ejemplos_sql2;
CREATE DATABASE ejemplos_sql2;

\c ejemplos_sql2

create table empleade (legajo int, nombre text, apellido text, departamento text, salario int);
create table profesore (legajo int, nombre text, apellido text, departamento text, salario int);
create table proyecto (proyecto_id int, nombre text, departamento text, fecha_inicio date);
create table empleade_proyecto (legajo int, proyecto_id int);
create table materia (nombre text);
create table parcial (materia_id text, parcial_id int, etapa text, profesore_id int);

alter table empleade add constraint empleade_pk primary key (legajo);
alter table profesore add constraint profesore_pk primary key (legajo);
alter table proyecto add constraint proyecto_pk primary key (proyecto_id);
alter table empleade_proyecto add constraint empleade_proyecto_pk primary key (legajo, proyecto_id);
alter table materia add constraint materia_pk primary key (nombre);
alter table parcial add constraint parcial_pk primary key (parcial_id);

alter table empleade_proyecto add constraint empleade_proyecto_legajo_fk foreign key (legajo) references empleade (legajo);
alter table empleade_proyecto add constraint empleade_proyecto_proyecto_id_fk foreign key (proyecto_id) references proyecto (proyecto_id);
alter table parcial add constraint parcial_profesore_id_fk foreign key (profesore_id) references profesore (legajo);

insert into empleade values (1234, 'Lionel', 'Messi', 'sistemas', 500000);
insert into empleade values (2345, 'Rodrigo', 'De Paul', 'administracion', 100000);
insert into empleade values (3456, 'Lautaro', 'Martinez', 'sistemas', 200000);
insert into empleade values (4567, 'Julian', 'Alvarez', 'finanzas', 150000);
insert into empleade values (5678, 'Emiliano', 'Martinez', 'finanzas', 250000);
insert into empleade values (6789, 'Angel', 'Correa', 'administracion', 100000);
insert into empleade values (7890, 'Angel', 'Di Maria', 'sistemas', 400000);

insert into profesore values (2222, 'Daniel', 'Bertaccini', 'sistemas', 500000);
insert into profesore values (3333, 'Hernan', 'Rondelli', 'sistemas', 500000);

insert into parcial values ('Base de Datos', 1, 'Primer Parcial', 2222);
insert into parcial values ('Base de Datos', 2, 'Segundo Parcial', 2222);
insert into parcial values ('Base de Datos', 3, 'Final Parcial', 2222);
insert into parcial values ('Base de Datos', 4, 'Primer Parcial', 3333);
insert into parcial values ('Base de Datos', 5, 'Segundo Parcial', 3333);

insert into materia values ('Base de Datos');

insert into proyecto values (1, 'Inversiones off-shore', 'finanzas', '2018-08-12');
insert into proyecto values (2, 'Mejoras SIU', 'sistemas', '2023-03-01');

insert into empleade_proyecto values (1234, 1);
insert into empleade_proyecto values (4567, 1);
insert into empleade_proyecto values (5678, 1);
insert into empleade_proyecto values (1234, 2);
insert into empleade_proyecto values (3456, 2);
insert into empleade_proyecto values (7890, 2);

-- tablas para ( recuperatorio - primer semestre 2023 )

-- Crear la tabla "usuario"

create table usuario (
    id int,
    nombre text,
    apellido text,
    nombre_plan text
);

alter table usuario add constraint usuario_pk primary key (id);

-- Crear la tabla "serie"
create table serie (
    id int,
    nombre text,
    descripcion text
);

alter table serie add constraint serie_pk primary key (id);

-- Crear la tabla "vista"
create table vista (
    id int,
    id_usuario int,
    id_serie int,
    fecha date
);

alter table vista add constraint vista_pk primary key (id);
alter table vista add constraint vista_id_usario_fk foreign key (id_usuario) references usuario (id);
alter table vista add constraint vista_id_serie_fk foreign key (id_serie) references serie (id);

-- Insertar datos en la tabla "usuario"
insert into usuario (id, nombre, apellido, nombre_plan)
values
    (626, 'Juan', 'Perez', 'Plan A'),
    (200, 'Maria', 'Gomez', 'Plan B'),
    (627, 'Luis', 'Rodriguez', 'Plan A'),
    (201, 'Laura', 'Diaz', 'Plan C'),
    (628, 'Pedro', 'Lopez', 'Plan B'),
    (629, 'Ana', 'Martinez', 'Plan A'),
    (630, 'Sofia', 'Fernandez', 'Plan C'),
    (631, 'Carlos', 'Gonzalez', 'Plan B'),
    (632, 'Roberto', 'Sanchez', 'Plan A'),
    (633, 'Andrea', 'Torres', 'Plan B'),
    (634, 'Javier', 'Rojas', 'Plan C'),
    (635, 'Fernando', 'Gutierrez', 'Plan A'),
    (636, 'Valeria', 'Lopez', 'Plan B'),
    (637, 'Eduardo', 'Ramirez', 'Plan C');

-- Insertar datos en la tabla "serie"
insert into serie (id, nombre, descripcion)
values
    (1, 'Serie 1', 'Descripción de la Serie 1'),
    (2, 'Serie 2', 'Descripción de la Serie 2'),
    (3, 'Serie 3', 'Descripción de la Serie 3'),
    (4, 'Serie 4', 'Descripción de la Serie 4'),
    (5, 'Serie 5', 'Descripción de la Serie 5'),
    (6, 'Serie 6', 'Descripción de la Serie 6'),
    (7, 'Serie 7', 'Descripción de la Serie 7'),
    (8, 'Serie 8', 'Descripción de la Serie 8'),
    (9, 'Serie 9', 'Descripción de la Serie 9'),
    (10, 'Serie 10', 'Descripción de la Serie 10'),
    (11, 'Serie 11', 'Descripción de la Serie 11'),
    (12, 'Serie 12', 'Descripción de la Serie 12'),
    (13, 'Serie 13', 'Descripción de la Serie 13'),
    (14, 'Serie 14', 'Descripción de la Serie 14');

-- Insertar datos en la tabla "vista"
insert into vista (id, id_usuario, id_serie, fecha)
values
    (1, 626, 7, '2023-01-10'),
    (2, 626, 2, '2023-02-15'),
    (3, 200, 1, '2023-01-12'),
    (4, 200, 3, '2023-02-20'),
    (5, 627, 2, '2023-01-14'),
    (6, 627, 4, '2023-02-25'),
    (7, 626, 3, '2023-01-18'),
    (8, 201, 5, '2023-03-01'),
    (9, 628, 4, '2023-01-20'),
    (10, 629, 1, '2023-01-22'),
    (11, 200, 4, '2023-02-28'),
    (12, 631, 3, '2023-03-10'),
    (13, 632, 4, '2023-03-15'),
    (14, 200, 5, '2023-03-18'),
    (15, 634, 1, '2023-03-22'),
    (16, 635, 2, '2023-03-30'),
    (17, 636, 3, '2023-04-05'),
    (18, 637, 4, '2023-04-10');

-- Crear la tabla "artista"
create table artista (
    id int,
    nombre text,
    comp_disco text,
    direccion text
);

alter table artista add constraint artista_pk primary key (id);

-- Crear la tabla "estadio"
create table estadio (
    id int,
    nombre text,
    capacidad int,
    direccion text
);

alter table estadio add constraint estadio_pk primary key (id);

-- Crear la tabla "concierto"
create table concierto (
    id_artista int,
    id_estadio int,
    fecha date
);
alter table concierto add constraint concierto_id_artista_fk foreign key (id_artista) references artista (id);
alter table concierto add constraint concierto_id_estadio_fk foreign key (id_estadio) references estadio (id);



-- Llenar la tabla "artista" con datos de muestra
insert into artista (id, nombre, comp_disco, direccion)
values
    (1, 'Artista 1', 'Discográfica A', 'Dirección 1'),
    (2, 'Artista 2', 'Discográfica B', 'Dirección 2'),
    (3, 'Artista 3', 'Discográfica C', 'Dirección 3'),
    (4, 'Artista 4', 'Discográfica A', 'Dirección 4'),
    (5, 'Artista 5', 'Discográfica B', 'Dirección 5');

-- Llenar la tabla "estadio" con datos de muestra
insert into estadio (id, nombre, capacidad, direccion)
values
    (1, 'Estadio 1', 60000, 'Dirección Estadio 1'),
    (2, 'Estadio 2', 70000, 'Dirección Estadio 2'),
    (3, 'Estadio 3', 55000, 'Dirección Estadio 3'),
    (4, 'Estadio 4', 40000, 'Dirección Estadio 4'),
    (5, 'Estadio 5', 90000, 'Dirección Estadio 5');

-- Llenar la tabla "concierto" con datos de muestra
insert into concierto (id_artista, id_estadio, fecha)
values
    (1, 1, '2023-01-10'),
    (1, 2, '2023-01-20'),
    (1, 3, '2023-02-05'),
    (1, 4, '2023-02-15'),
    (2, 2, '2023-02-25'),
    (2, 4, '2023-03-10'),
    (3, 2, '2023-03-15'),
    (3, 3, '2023-03-20'),
    (1, 5, '2023-04-05'),
    (4, 5, '2023-04-15');
