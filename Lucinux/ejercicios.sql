-- empleados que trabajan en todos los proyectos
-- Traeme los empleados donde "no existe"  un proyecto en donde "no" trabajen.

select distinct legajo 
from   empleade_proyecto ep1
where  not exists 
          (select 1 
           from   proyecto p
           where  not exists 
                     (select 1
                      from empleade_proyecto ep2
                      where ep2.legajo = ep1.legajo and ep2.proyecto_id = p.proyecto_id));
                      
                      
-- Profesores que tomaron todos los parciales.
-- Traeme los profesores en donde no "existe" pacial que "no" hayan tomado.

select apellido, nombre from profesore
where legajo =
(
select distinct profesore_id 
from   parcial p1
where  not exists 
          (select 1 
           from   parcial pneutra
           where  not exists 
                     (select 1
                      from parcial p2
                      where p2.profesore_id = p1.profesore_id and p2.etapa = pneutra.etapa))
                      );

-- nombre de series que vio el 626 pero que no vio el 200

select s.nombre from serie s 
	where id in ( 
		select id_serie from vista where id_usuario = 626 and
			id_serie not in ( select id_serie from vista where id_usuario = 200 ));

-- obtener de cada plan la cantidad de series que vieron los usuarios de ese plan
-- esos planes deben ser mas de 150000 ( cambiado ) 

select nombre_plan, count(v1.id_serie) from usuario u1, vista v1
where u1.id = v1.id_usuario 
group by nombre_plan
having count ( distinct id_usuario ) > 4;

-- obtener los nombres de los artistas que dieron al menos 
-- 20 conciertos en todos los estadios

select a.nombre, comp_disco, count(distinct c.id_estadio) AS cant_conciertos from
	artista a, concierto c, estadio e
	where a.id = c.id_artista and e.id = c.id_estadio
	group by a.nombre, comp_disco
	having count ( distinct c.id_estadio ) >= 3
	order by a.nombre asc;
	
-- obtener los nombres de los artistas que dieron conciertos en todos los
-- estadios que tengan capacidad mayor a 50000
select distinct a.nombre from artista a, concierto c, estadio e
	where e.id = c.id_estadio and a.id = c.id_artista and
		e.capacidad > 50000 and
			not exists (select 1 from 
				artista a1, concierto c1, estadio e1
				where e1.id = c1.id_estadio and a1.id = c1.id_artista and
				e1.capacidad > 50000 and
				not exists (select 1 from 
					artista a2, concierto c2, estadio e2
					where e2.id = c2.id_estadio and a2.id = c2.id_artista and
					e2.capacidad > 50000 and 
					a2.id = a.id and e1.id = e2.id ));

-- reformateando 					
	select nombre from artista 
	where id in (
	select id_artista from concierto c
	where not exists (select 1 from estadio e
				where e.capacidad > 50000 and
				not exists (select 1 from 
					concierto c1
					where  c.id_artista = c1.id_artista and
					c.id_artista = c1.id_artista and c1.id_estadio = e.id )));		
	

-- otro reformat
 select a.nombre from artista a 
	where not exists ( select 1 from estadio e
		where capacidad > 50000 and not exists ( select 1 from concierto c
			where a.id = c.id_artista and c.id_estadio = e.id ));

